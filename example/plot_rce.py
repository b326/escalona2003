"""
An vs. iPAR
===========

Plot daily carbon uptake vs. intercepted PAR to compare to fig4.
"""
import matplotlib.pyplot as plt
import pandas as pd

from escalona2003 import pth_clean

# read data
df = pd.read_csv(pth_clean / "rce.csv", sep=";", comment="#")

# plot data
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(12, 6), squeeze=False)

for j, ((variety, year), ydf) in enumerate(df.groupby(['variety', 'year'])):
    ax = axes[0, j]
    ax.set_title(f"{variety} {year:d}")
    for i, stage in enumerate(["pea_size", "veraison", "ripeness"]):
        sdf = ydf[ydf['stage'] == stage]
        ax.plot(sdf['ipar'], sdf['an'], 'o', label=f"{stage}")

    ax.legend(loc='upper left')
    ax.set_ylim(0, 0.7)
    ax.set_xlim(0, 60)
    ax.set_xlabel("iPAR [mol photon m-2 day-1] (corrected)")
    ax.set_ylabel("Assimilation [molC m-2 day-1] (corrected)")

fig.tight_layout()
plt.show()
