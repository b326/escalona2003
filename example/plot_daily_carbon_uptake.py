"""
Daily carbon uptake
===================

Plot daily carbon uptake histograms to compare to fig3.
"""
import matplotlib.pyplot as plt
import pandas as pd

from escalona2003 import pth_clean

# read data
df = pd.read_csv(pth_clean / "daily_carbon_uptake.csv", sep=";", comment="#")

# plot data
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(12, 6), squeeze=False)

for j, ((variety, year), ydf) in enumerate(df.groupby(['variety', 'year'])):
    ax = axes[0, j]
    ax.set_title(f"{variety} {year:d}")
    for i, stage in enumerate(["pea_size", "veraison", "ripeness"]):
        sdf = ydf[ydf['stage'] == stage]
        ax.bar(sdf['zone'].values + (i - 1) * 0.25, sdf['photo_net'].values, width=0.15, label=f"{stage}")

    ax.legend(loc='upper left')
    ax.set_ylim(0, 8)
    ax.set_xlabel("zone")
    ax.set_ylabel("Assimilation [gC m-2 day-1]")

fig.tight_layout()
plt.show()
